const renderChildPage = (element) => {
    const el = document.querySelectorAll(element)[0];
    const text = document.createElement('span');
    text.textContent = '这是子页面';

    el.appendChild(text);
};

window.renderChildPage = renderChildPage;

export {renderChildPage}
